import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/animation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'package:provider/provider.dart';

import './login-register/stateManegment.dart';
import 'userStatus.dart';

final FirebaseAuth _firebaseAuth = FirebaseAuth.instance;

class LandingPage extends StatefulWidget {
  LandingPage() : super();

  @override
  _LandingPageState createState() => _LandingPageState();
}

class _LandingPageState extends State<LandingPage>
    with TickerProviderStateMixin {
  final st = new FlutterSecureStorage();
  bool showlandingAnimation = true;

  Animation<double> animInc;
  Animation<double> animDec;
  AnimationController controller;
  double animOpacity = 1.0;
  int animCounter = 0;
  double showError = 0.0;
  var userStatus;

  void animExpandLogo() {
    // width height animation
    controller = AnimationController(
        duration: const Duration(milliseconds: 600), vsync: this);
    animInc = Tween<double>(begin: 0, end: 128).animate(controller)
      ..addListener(() {
        setState(() {
          if (animInc.value == 128) {
            animDecOpacity();
          }
        });
      });
    controller.forward();
  }

  void animDecOpacity() {
    controller.dispose();
    controller =
        AnimationController(duration: const Duration(seconds: 1), vsync: this);
    animDec = Tween<double>(begin: 1.0, end: 0.6).animate(controller)
      ..addListener(() {
        setState(() {
          if (animDec.value == 0.6) {
            animIncOpacity();
          } else {
            animOpacity = animDec.value;
          }
        });
      });
    controller.forward();
  }

  void animIncOpacity() {
    controller.dispose();
    controller =
        AnimationController(duration: const Duration(seconds: 1), vsync: this);
    animDec = Tween<double>(begin: 0.6, end: 1.0).animate(controller)
      ..addListener(() {
        setState(() {
          animCounter++;
          if (animCounter >= 6) {
            if (userStatus == userStats.login) {
              Navigator.pushNamed(context, "/loggedIn");
            } else if (userStatus == userStats.networkError) {
              showlandingAnimation = true;
            } else if (userStatus == userStats.logout) {
              showlandingAnimation = false;
            } else {
              animCounter = 0;
            }
          }
          if (animDec.value == 1.0) {
            animDecOpacity();
          } else {
            animOpacity = animDec.value;
          }
        });
      });
    controller.forward();
  }

  void userCredentialControl() async {
    String accessToken = await st.read(key: "accessToken");
    String idToken = await st.read(key: "idToken");
    print(accessToken);
    print(idToken);
    if (accessToken == null && idToken == null) {
      userStatus = userStats.logout;
    } else {
      AuthCredential credential;
      FirebaseUser user;

      try {
        credential = GoogleAuthProvider.getCredential(
          accessToken: accessToken,
          idToken: idToken,
        );
      } on PlatformException catch (e) {
        print("googleCredential error: ");
        print(e.code);
        showError = 1.0;
        setState(() {});
      }

      try {
        user = (await _firebaseAuth.signInWithCredential(credential)).user;
        print(user);
        if (user != null) {
          userStatus = userStats.login;
        } else {
          userStatus = userStats.logout;
        }
        //print("signed in " + user.displayName);
        //print(user);
      } on PlatformException catch (e) {
        if (e.code == "ERROR_INVALID_CREDENTIAL") {
          userStatus = userStats.logout;
        } else {
          userStatus = userStats.networkError;
          showError = 1.0;
        }
        setState(() {});
      }
    }
  }

  @override
  void initState() {
    super.initState();
    animExpandLogo();
    userCredentialControl();
  }

  @override
  void dispose() {
    controller.dispose();
    super.dispose();
  }

  Widget LandingIcon() {
    return Container(
      color: Color.fromRGBO(150, 225, 255, 1.0),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          Center(
            child: Opacity(
              opacity: animOpacity,
              child: Image.asset(
                'assets/island.png',
                width: animInc.value,
                height: animInc.value,
              ),
            ),
          ),
          const SizedBox(
            height: 10,
          ),
          SizedBox(
              child: Opacity(
                  opacity: showError,
                  child: Text("Lütfen bağlantınızı kontrol ediniz..",
                      style: TextStyle(
                          fontSize: 10,
                          decoration: TextDecoration.none,
                          color: Colors.black,
                          fontFamily: "arial"))))
        ],
      ),
    );
  }

  Widget LandingLogin() {
    return Consumer<UserData>(builder: (context, cart, child) {
      return Scaffold(
        body: Center(
            child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
              const SizedBox(height: 50),
              SizedBox(
                width: 220.0,
                height: 50.0,
                child: FlatButton(
                  shape: new RoundedRectangleBorder(
                      borderRadius: new BorderRadius.circular(12.0),
                      side: BorderSide(color: Colors.red)),
                  onPressed: () async {
                    final GoogleSignIn _googleSignIn = new GoogleSignIn();
                    GoogleSignInAccount googleUser;
                    GoogleSignInAuthentication googleAuth;
                    AuthCredential credential;
                    FirebaseUser user;

                    try {
                      googleUser = await _googleSignIn.signIn();
                    } on PlatformException catch (e) {
                      print("googlesingin error: ");
                      print(e.code);
                      showError = 1.0;
                      setState(() {});
                    }

                    try {
                      googleAuth = await googleUser.authentication;
                      credential = GoogleAuthProvider.getCredential(
                        accessToken: googleAuth.accessToken,
                        idToken: googleAuth.idToken,
                      );
                    } on PlatformException catch (e) {
                      print("googleCredential error: ");
                      print(e);
                    }

                    try {
                      user =
                          (await _firebaseAuth.signInWithCredential(credential))
                              .user;
                      //print("signed in " + user.displayName);
                      //print(user);
                    } on PlatformException catch (error) {
                      print("firebase singinError: ");
                      print(error.code);
                      showError = 1.0;
                      setState(() {});
                    }

                    await cart.setUserCredentials(
                        googleAuth.accessToken, googleAuth.idToken);
                    await cart.setUserData(user);
                    if (user != null) {
                      Navigator.pushNamed(context, "/loggedIn");
                    }
                  },
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Image.asset(
                        'assets/ua_google.png',
                      ),
                    ],
                  ),
                  color: Colors.white,
                  splashColor: Colors.red,
                ),
              ),
              const SizedBox(height: 15),
              SizedBox(
                width: 220.0,
                height: 50.0,
                child: FlatButton(
                  shape: new RoundedRectangleBorder(
                      borderRadius: new BorderRadius.circular(12.0),
                      side: BorderSide(color: Colors.blue)),
                  onPressed: () {},
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Image.asset(
                        'assets/ua_facebook.png',
                      ),
                    ],
                  ),
                  color: Colors.white,
                  splashColor: Colors.blue,
                ),
              ),
              const SizedBox(height: 15),
              SizedBox(
                width: 220.0,
                height: 50.0,
                child: FlatButton(
                  shape: new RoundedRectangleBorder(
                      borderRadius: new BorderRadius.circular(12.0),
                      side: BorderSide(color: Colors.lightBlueAccent)),
                  onPressed: () {},
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Image.asset(
                        'assets/ua_twitter.png',
                      ),
                    ],
                  ),
                  color: Colors.white,
                  splashColor: Colors.lightBlueAccent,
                ),
              ),
              const SizedBox(height: 15),
              SizedBox(
                child: Opacity(
                  opacity: showError,
                  child: Text(
                    "Lütfen bağlantınızı kontrol ediniz..",
                  ),
                ),
              )
            ])),
      );
    });
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    if (showlandingAnimation) {
      return LandingIcon();
    } else {
      return LandingLogin();
    }
  }
}
