import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'login/login.dart';
import 'login-register/authentication.dart';
import 'login-register/stateManegment.dart';
import 'landingPage.dart';

BaseAuth auth = new Auth();

void main() {
  runApp(MultiProvider(
    providers: [
      ChangeNotifierProvider(builder: (context) => UserData(auth: auth)),
    ],
    child: MyApp(),
  ));
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'ÜtopyaAdası',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      //home: LoginPage(),
      initialRoute: '/',
      routes: {
        '/': (context) => LandingPage(),
        '/loggedIn': (context) => LoggedIn(),
        //'/emailRegister': (context) => EmailRegister(),
      },
    );
  }
}
