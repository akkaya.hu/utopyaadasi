import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_database/firebase_database.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';

import 'authentication.dart';

class UserData extends ChangeNotifier {
  final BaseAuth auth;

  final st = new FlutterSecureStorage();
  final db = FirebaseDatabase.instance;
  String displayName = "";
  String email = "";
  String uid = "";
  String photoUrl = "";
  String accessToken = "";
  String idToken = "";

  UserData({Key key, this.auth}) {
    () async {
      //print(this.auth.getCurrentUser());
      email = await st.read(key: "email");
      displayName = await st.read(key: "displayName");
      uid = await st.read(key: "uid");
      photoUrl = await st.read(key: "photoUrl");
    }();
  }

  Future<bool> setUserCredentials(String accessTkn, String idTkn) async {
    // önce eskisini silsin sonra yenisini kaydetsin.
    await st.deleteAll();
    await st.write(key: "accessToken", value: accessTkn);
    await st.write(key: "idToken", value: idTkn);

    accessToken = accessToken;
    idToken = idToken;
    return true;
  }

  Future<bool> setUserData(FirebaseUser user) async {
    displayName = user.displayName;
    email = user.email;
    uid = user.uid;
    photoUrl = user.photoUrl;

    // önce eskisini silsin sonra yenisini kaydetsin.
    await st.write(key: "displayName", value: user.displayName);
    await st.write(key: "email", value: user.email);
    await st.write(key: "uis", value: user.uid);
    await st.write(key: "photoUrl", value: user.photoUrl);
    return true;
  }

  Future<Map> read() async {
    Map<String, String> data = await st.readAll();
    return data;
  }

  Future<bool> logOutUser() async {
    await auth.signOut();
    await st.deleteAll();
    notifyListeners();
  }
}
