import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import './profile/profile.dart';
import '../login-register/stateManegment.dart';

class LoggedIn extends StatefulWidget {
  @override
  _LoggedInState createState() => _LoggedInState();
}

class _LoggedInState extends State<LoggedIn> {
  int _selectedIndex = 0;
  static TextStyle optionStyle = TextStyle(fontSize: 30, fontWeight: FontWeight.bold);
  List<Widget> _widgetOptions = <Widget>[
    Text(
      'Index 0: Home',
      style: optionStyle,
    ),
    Text(
      'Index 1: Content',
      style: optionStyle,
    ),
    new Profile(),
  ];

  void _onItemTapped(int index) {
    setState(() {
      _selectedIndex = index;
    });
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Consumer<UserData>(builder: (context, cart, child) {
      return Scaffold(
        body: Center(
          child: _widgetOptions.elementAt(_selectedIndex),
        ),

        /*Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            Center(child: Text("LoggedIn !!")),
            Padding(
              padding: const EdgeInsets.symmetric(vertical: 0.0),
              child: RaisedButton(
                color: Colors.white,
                textColor: Colors.blue,
                onPressed: () async {
                  await cart.logOutUser();
                  Navigator.popUntil(context, ModalRoute.withName('/loggedIn'));
                  Navigator.pushReplacementNamed(context, '/');
                },
                child: SizedBox(
                  width: 200,
                  child: Text(
                    'logOut',
                    textAlign: TextAlign.center,
                    style: TextStyle(
                      fontSize: 12,
                    ),
                  ),
                ),
              ),
            ),
          ],
        ),*/
        bottomNavigationBar: BottomNavigationBar(
          showSelectedLabels: false,
          showUnselectedLabels: false,
          items: const <BottomNavigationBarItem>[
            BottomNavigationBarItem(
              icon: Icon(Icons.home),
              title: Text(''),
            ),
            BottomNavigationBarItem(
              icon: Icon(Icons.apps),
              title: Text(''),
            ),
            BottomNavigationBarItem(
              icon: Icon(Icons.account_circle),
              title: Text(''),
            ),
          ],
          currentIndex: _selectedIndex,
          selectedItemColor: Color.fromRGBO(150, 225, 255, 1.0),
          onTap: _onItemTapped,
        ),
      );
    });
  }
}
