import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../../login-register/stateManegment.dart';

class Profile extends StatefulWidget {
  @override
  _ProfileState createState() => _ProfileState();
}

class _ProfileState extends State<Profile> {
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Consumer<UserData>(builder: (context, cart, child) {
      return Container(
          child: Center(
              child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.start,
            children: <Widget>[
              SizedBox(height: 30),
              Container(
                margin: EdgeInsets.fromLTRB(20, 30, 0, 0),
                child: CircleAvatar(
                    backgroundImage: NetworkImage(cart.photoUrl),
                    minRadius: 50,
                    maxRadius: 50),
              ),
              SizedBox(height: 50),
              Container(
                width: 300,
                padding: EdgeInsets.fromLTRB(20, 10, 20, 10),
                decoration: BoxDecoration(
                  color: Colors.teal,
                    borderRadius: new BorderRadius.all(Radius.circular(5.0))
                ),
                child: Column(
                  children: <Widget>[
                    Text(cart.displayName,
                        style: TextStyle(color: Colors.white, fontSize: 18)),
                    Text(cart.email,
                        style: TextStyle(color: Colors.white, fontSize: 18)),
                  ],
                ),
              ),
              RaisedButton(

                  color: Colors.white,
                  textColor: Colors.blue,
                  onPressed: () async {
                    await cart.logOutUser();
                    Navigator.popUntil(
                        context, ModalRoute.withName('/loggedIn'));
                    Navigator.pushReplacementNamed(context, '/');
                  },
                  child: SizedBox(
                    width: 200,
                    child: Text(
                      'logOut',
                      textAlign: TextAlign.center,
                      style: TextStyle(
                        fontSize: 12,
                      ),
                    ),
                  )),
            ],
          )));
    });
  }
}
